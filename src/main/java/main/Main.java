package main;

import gui.AsteroidsApp;
import javax.swing.SwingUtilities;

/**
 * Launches the program.
 * 
 * @author Max Jonsson
 * @version 2020-02-20
 */
public class Main {
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new AsteroidsApp());
	}
	
}
