package visitor;

/**
 * Visitable interface for GameObjects.
 *
 * @author Max Jonsson
 * @version 2020-02-24
 */
public interface VisitableGameObject {
    public void accept(GameObjectVisitor visitor);
}
