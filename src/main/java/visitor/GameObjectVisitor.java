package visitor;

import domain.gameobjects.Asteroid;
import domain.gameobjects.Bullet;
import domain.gameobjects.SpaceShip;
import domain.gameobjects.Ufo;

/**
 * Visitor interface for visiting of GameObjects.
 *
 * @author Max Jonsson
 * @version 2020-02-24
 */
public interface GameObjectVisitor {
    public void visitAsteroidSmall(Asteroid asteroid);
    public void visitAsteroidMedium(Asteroid asteroid);
    public void visitAsteroidBig(Asteroid asteroid);
    public void visitBullet(Bullet bullet);
	public void visitSpaceShip(SpaceShip spaceShip);
	public void visitUfo(Ufo ufo);
}
