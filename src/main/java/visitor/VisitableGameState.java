package visitor;

/**
 * Visitable interface for GameStates.
 * 
 * @author Max Jonsson
 * @version 2020-03-17
 */
public interface VisitableGameState {
    public void accept(GameStateVisitor visitor);
}
