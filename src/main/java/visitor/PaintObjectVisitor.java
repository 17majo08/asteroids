package visitor;

import domain.gameobjects.Asteroid;
import domain.gameobjects.Bullet;
import domain.gameobjects.SpaceShip;
import domain.gameobjects.Ufo;
import domain.factory.ShapeFactory;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 * GameObjectVisitor implementation that paints GameObjects to a graphical
 * context.
 * 
 * @author Max Jonsson
 * @version 2020-02-24
 */
public class PaintObjectVisitor implements GameObjectVisitor {
	private final Graphics2D g2d;
	private final ShapeFactory shapeFactory;
	
	public PaintObjectVisitor(Graphics2D g2d) {
		this.g2d = g2d;
		this.shapeFactory = new ShapeFactory();
	}
	
	@Override
	public void visitAsteroidSmall(Asteroid asteroid) {
		g2d.draw(shapeFactory.ofAsteroidSmall(asteroid));
	}
	
	@Override
	public void visitAsteroidMedium(Asteroid asteroid) {
		g2d.draw(shapeFactory.ofAsteroidMedium(asteroid));
	}
	
	@Override
	public void visitAsteroidBig(Asteroid asteroid) {
		g2d.draw(shapeFactory.ofAsteroidBig(asteroid));
	}

	@Override
	public void visitBullet(Bullet bullet) {
		g2d.fill(shapeFactory.ofBullet(bullet));
	}

	@Override
	public void visitSpaceShip(SpaceShip spaceShip) {
		spaceShip.getBullets().forEach(bullet -> bullet.accept(this));
		
		AffineTransform old = g2d.getTransform();
		g2d.rotate(-Math.toRadians(spaceShip.getFacingAngle()), spaceShip.getX(), spaceShip.getY());
		g2d.draw(shapeFactory.ofSpaceShip(spaceShip));
		g2d.setTransform(old);
	}

	@Override
	public void visitUfo(Ufo ufo) {
		Color oldColor = g2d.getColor();
		
		g2d.setColor(Color.RED);
		ufo.getBullets().forEach(bullet -> bullet.accept(this));
		
		g2d.setColor(new Color(57, 255, 20));
		g2d.draw(shapeFactory.ofUfo(ufo));
		
		g2d.setColor(oldColor);
	}
    
}
