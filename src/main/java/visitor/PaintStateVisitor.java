package visitor;

import controller.Controller;
import gui.Bounds;
import java.awt.Font;
import java.awt.Graphics2D;

/**
 * GameStateVisitor implementation that paints to a graphical context the content 
 * that is representative for a certain GameState.
 *
 * @author Max Jonsson
 * @version 2020-02-27
 */
public class PaintStateVisitor implements GameStateVisitor {
	private final Graphics2D g2d;
	private final Controller controller;
	
	public PaintStateVisitor(Graphics2D g2d) {
		this.g2d = g2d;
		this.controller = Controller.getInstance();
	}
	
	/**
	 * Paints all GameObjects to the graphical context.
	 */
	private void paintGameObjects() {
		GameObjectVisitor drawVisitor = new PaintObjectVisitor(g2d);
		controller.getGameObjects().forEach(obj -> obj.accept(drawVisitor));
	}
	
	private int getStringWidth(String s) {
		return g2d.getFontMetrics().stringWidth(s);
	}
	
	/**
	 * Paints a string to the graphical context.
	 * @param text The string.
	 * @param xStart The x-coordinate.
	 * @param yStart The y-coordinate.
	 */
	private void paintText(String text, int xStart, int yStart) {
		g2d.drawString(text, xStart, yStart);
	}
	
	/**
	 * Paints a small scoreboard at the top of the screen to this graphical 
	 * context.
	 */
	private void paintScore() {
		Font oldFont = g2d.getFont();
		g2d.setFont(new Font(oldFont.getName(), oldFont.getStyle(), 10));
		
		int score = controller.getCurrentScore();
		int highscore = controller.getHighScore();
		String scoreString = String.format("Score: %d High Score: %d", score, highscore);
		
		int xStart = Bounds.WIDTH / 2 - getStringWidth(scoreString) / 2;
		int yStart = 10;
		paintText(scoreString, xStart, yStart);
		
		g2d.setFont(oldFont);
	}
	
	@Override
	public void visitStarted() {
		paintGameObjects();
		// paintScore();
	}
	
	@Override
	public void visitNotStarted() {
		String text = "Press space to start";
		int xStart = Bounds.WIDTH / 2 - getStringWidth(text) / 2;
		int yStart = Bounds.HEIGHT / 2;
		
		paintText(text, xStart, yStart);
	}
	
	@Override
	public void visitGameOver() {
		paintGameObjects();
		
		int score = controller.getCurrentScore();
		int highscore = controller.getHighScore();
		
		String text = score < highscore ? "Game Over!" : "New High Score!";
		int xStart = Bounds.WIDTH / 2 - getStringWidth(text) / 2;
		int yStart = Bounds.HEIGHT / 2;
		paintText(text, xStart, yStart);
		
		Font fontOld = g2d.getFont();
		g2d.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		
		text = String.format("Score: %d", score);
		xStart = Bounds.WIDTH / 2 - getStringWidth(text) / 2;
		yStart = Bounds.HEIGHT / 2 + 30;
		paintText(text, xStart, yStart);
		
		text = String.format("High Score: %d", highscore);
		xStart = Bounds.WIDTH / 2 - getStringWidth(text) / 2;
		yStart = Bounds.HEIGHT / 2 + 60;
		paintText(text, xStart, yStart);
		
		g2d.setFont(fontOld);
	}
	
}
