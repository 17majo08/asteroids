package visitor;

/**
 * Visitor interface for visiting of GameStates.
 *
 * @author Max Jonsson
 * @version 2020-02-27
 */
public interface GameStateVisitor {
    public void visitStarted();
	public void visitNotStarted();
	public void visitGameOver();
}
