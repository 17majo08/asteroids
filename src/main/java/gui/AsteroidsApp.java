package gui;

import javax.swing.JFrame;

/**
 * Main Frame of the application.
 * 
 * @author Max Jonsson
 * @version 2020-02-18
 */
public class AsteroidsApp {
    private final JFrame window;
	private final Canvas canvas;
	
	public AsteroidsApp() {
		this.window = new JFrame();
		this.canvas = new Canvas();
		window.addKeyListener(canvas);
		
		window.setSize(Bounds.WIDTH, Bounds.HEIGHT);
		window.setResizable(false);
		
		window.add(canvas);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
	}
	
}
