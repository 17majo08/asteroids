package gui;

import controller.Controller;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import visitor.GameStateVisitor;
import visitor.PaintStateVisitor;

/**
 * Main Canvas of the application.
 *
 * @author Max Jonsson
 * @version 2020-02-18
 */
public class Canvas extends JPanel implements KeyListener {
	private final Controller controller;
	
	public Canvas() {
		controller = Controller.getInstance();
		controller.addObserver(() -> repaint());
		
		setBackgroundColor(new Color(27, 30, 35));
	}
	
	private void setBackgroundColor(Color color) {
		this.setBackground(color);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setColor(Color.WHITE);
		g2d.setStroke(new BasicStroke(2));
		g2d.setFont(new Font("Lucida Grande", Font.PLAIN, 40));
		
		GameStateVisitor visitor = new PaintStateVisitor(g2d);
		controller.getGameState().accept(visitor);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_UP:
				controller.onUpPressed(); break;
			case KeyEvent.VK_LEFT:
				controller.onLeftPressed(); break;
			case KeyEvent.VK_RIGHT:
				controller.onRightPressed(); break;
			case KeyEvent.VK_SPACE:
				controller.onSpacePressed(); break;
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_UP:
				controller.onUpReleased(); break;
			case KeyEvent.VK_LEFT:
				controller.onLeftReleased(); break;
			case KeyEvent.VK_RIGHT:
				controller.onRightReleased(); break;
			case KeyEvent.VK_SPACE:
				controller.onSpaceReleased(); break;
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	
}
