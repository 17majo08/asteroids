package gui;

/**
 * Abstract class that provides system with the Bounds of its Frame.
 * 
 * @author Max Jonsson
 * @version 2020-02-21
 */
public abstract class Bounds {
    public static final int WIDTH = 800, HEIGHT = 800;
	
	/**
	 * Checks if a coordinate is within the Bounds.
	 * @param x The x-coordinate.
	 * @param y The y-coordinate.
	 * @return if true or false.
	 */
	public static boolean isWithin(int x, int y) {
		return x >= 0 && x <= WIDTH && y >= 0 && y <= HEIGHT;
	}
}
