package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Data access object to manage the storage of high score in a text file.
 *
 * @author Max Jonsson
 * @version 2020-03-18
 */
public class HighScoreDAO {
	private static final String FILE_NAME = "highscore.txt";
	
	/**
	 * Retrieves the high score from the text file.
	 * @return Currently stored high score.
	 * @throws IllegalStateException on error during reading.
	 */
	public int readHighScore() {
		try {
			File file = new File(FILE_NAME);
			BufferedReader reader = new BufferedReader(new FileReader(file));
			return Integer.parseInt(reader.readLine());
		} catch (IOException | NumberFormatException e) {
			throw new IllegalStateException(String.format("Error during reading. %s", e.getMessage()));
		}
	}
	
	/**
	 * Store the current score as high score.
	 * @param score The current score.
	 * @throws IllegalStateException on error during writing.
	 */
	public void writeHighScore(int score) {
		try {
			File file = new File(FILE_NAME);
			file.createNewFile();
			
			try (FileWriter writer = new FileWriter(file)) {
				writer.write(Integer.toString(score));
			}
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Error during writing. %s", e.getMessage()));
		}
	}
	
}
