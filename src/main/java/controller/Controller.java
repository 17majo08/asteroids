package controller;

import domain.game.Game;
import domain.gameobjects.GameObject;
import domain.state.GameState;
import java.util.List;
import observer.Observer;

/**
 * The controller maps GUI actions to Game specific actions. It acts as a 
 * Facade for the system to access its domain objects.
 * 
 * @author Max Jonsson
 * @version 2020-02-24
 */
public class Controller {
	private static final Controller INSTANCE = new Controller();
	
	private final Game game;
	private boolean spaceIsPressed, upIsPressed;
	
	private Controller() {
		this.game = new Game();
	}
	
	public int getCurrentScore() {
		return game.getScore();
	}
	
	public int getHighScore() {
		return game.getHighScore();
	}
	
	public GameState getGameState() {
		return GameState.getGameState();
	}
	
	public void onSpacePressed() {
		if (!spaceIsPressed) {
			spaceIsPressed = true;
			
			if (game.isStarted())
				game.shoot();
			else
				game.startGame();
		}
	}
	
	public void onSpaceReleased() {
		spaceIsPressed = false;
	}
	
	public void onUpPressed() {
		if (!upIsPressed) {
			upIsPressed = true;
			game.dash();
		}
	}
	
	public void onUpReleased() {
		upIsPressed = false;
	}
	
	public void onLeftPressed() {
		game.setRotationDirection(game.getRotationDirection().onLeftDown());
	}
	
	public void onLeftReleased() {
		game.setRotationDirection(game.getRotationDirection().onLeftUp());
	}
	
	public void onRightPressed() {
		game.setRotationDirection(game.getRotationDirection().onRightDown());
	}
	
	public void onRightReleased() {
		game.setRotationDirection(game.getRotationDirection().onRightUp());
	}
	
	public List<GameObject> getGameObjects() {
		return game.getGameObjects();
	}
	
	public void addObserver(Observer observer) {
		game.addObserver(observer);
	}
	
	public static Controller getInstance() {
		return INSTANCE;
	}
	
}
