package domain.factory;

import domain.gameobjects.Asteroid;
import domain.gameobjects.AsteroidBig;
import domain.gameobjects.AsteroidMedium;
import domain.gameobjects.AsteroidSmall;
import domain.gameobjects.GameObject;
import domain.gameobjects.Ufo;
import gui.Bounds;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The SpawnFactory is used to generate a spawn of GameObjects.
 *
 * @author Max Jonsson
 * @version 2020-02-23
 */
public enum SpawnFactory {
	ASTEROID_BIG {
		@Override
		public List<Asteroid> create(double x, double y, int quantity) {
			return IntStream.range(0, quantity)
					.mapToObj(i -> new AsteroidBig(x, y, getRandomAngle()))
					.collect(Collectors.toList());
		}

		@Override
		public List<Asteroid> create(int quantity) {
			return IntStream.range(0, quantity)
					.mapToObj(i -> new AsteroidBig(0, getRandomHeight(), getRandomAngle()))
					.collect(Collectors.toList());
		}
	}, ASTEROID_MEDIUM {
		@Override
		public List<Asteroid> create(double x, double y, int quantity) {
			return IntStream.range(0, quantity)
					.mapToObj(i -> new AsteroidMedium(x, y, getRandomAngle()))
					.collect(Collectors.toList());
		}

		@Override
		public List<Asteroid> create(int quantity) {
			return IntStream.range(0, quantity)
					.mapToObj(i -> new AsteroidMedium(0, getRandomHeight(), getRandomAngle()))
					.collect(Collectors.toList());
		}
	}, ASTEROID_SMALL {
		@Override
		public List<Asteroid> create(double x, double y, int quantity) {
			return IntStream.range(0, quantity)
					.mapToObj(i -> new AsteroidSmall(x, y, getRandomAngle()))
					.collect(Collectors.toList());
		}

		@Override
		public List<Asteroid> create(int quantity) {
			return IntStream.range(0, quantity)
					.mapToObj(i -> new AsteroidSmall(0, getRandomHeight(), getRandomAngle()))
					.collect(Collectors.toList());
		}
	}, UFO {
		@Override
		public List<Ufo> create(double x, double y, int quantity) {
			return IntStream.range(0, quantity)
					.mapToObj(i -> new Ufo(x, y, getRandomAngle()))
					.collect(Collectors.toList());
		}

		@Override
		public List<Ufo> create(int quantity) {
			return IntStream.range(0, quantity)
					.mapToObj(i -> new Ufo(0, getRandomHeight(), getRandomAngle()))
					.collect(Collectors.toList());
		}
	};
	
	private static final double MAX_DEGREES = 360d;
	
	private static double getRandomAngle() {
		return Math.random() * SpawnFactory.MAX_DEGREES;
	}
	
	private static double getRandomHeight() {
		return Math.random() * Bounds.HEIGHT;
	}
	
	/**
	 * Creates objects at the x-coordinate 0 with a random y-coordinate.
	 * @param <T>
	 * @param quantity The amount of objects to produce.
	 * @return List of the produced objects.
	 */
	public abstract <T extends GameObject> List<T> create(int quantity);
	/**
	 * Creates objects at a specified coordinate
	 * @param <T>
	 * @param x The x-coordinate.
	 * @param y The y-coordinate.
	 * @param quantity The amount of objects to produce.
	 * @return List of the produced objects.
	 */
	public abstract <T extends GameObject> List<T> create(double x, double y, int quantity);
	
}
