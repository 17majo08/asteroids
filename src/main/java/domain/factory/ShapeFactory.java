package domain.factory;

import domain.gameobjects.Asteroid;
import domain.gameobjects.Bullet;
import domain.gameobjects.SpaceShip;
import domain.gameobjects.Ufo;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;

/**
 * The ShapeFactory creates the geometric shape representations for the concrete 
 * GameObjects. The Shape representation is primary used for printing in a 
 * javax.swing context.
 *
 * @author Max Jonsson
 * @version 2020-02-24
 */
public class ShapeFactory {
	
	public Shape ofSpaceShip(SpaceShip spaceShip) {
		final double center_x = spaceShip.getX();
		final double center_y = spaceShip.getY();
		final double radius = spaceShip.getRadius();
		
		Path2D path = new Path2D.Double();
		path.moveTo(center_x + radius, center_y);
		path.lineTo(center_x - radius, center_y - radius + 5.d);
		path.lineTo(center_x - radius + 5.d, center_y);
		path.lineTo(center_x - radius, center_y + radius - 5.d);
		path.closePath();
		
		return path;
	}
	
	public Shape ofBullet(Bullet bullet) {
		final double x = bullet.getX();
		final double y = bullet.getY();
		final double volume = bullet.getRadius();
		
		return new Ellipse2D.Double(
				x - volume,
				y - volume,
				volume * 2.d,
				volume * 2.d);
	}
	
	public Shape ofAsteroidSmall(Asteroid asteroid) {
		return ofAsteroid(asteroid, 2.d, 5.d);
	}
	
	public Shape ofAsteroidMedium(Asteroid asteroid) {
		return ofAsteroid(asteroid, 2.d, 12.d);
	}
	
	public Shape ofAsteroidBig(Asteroid asteroid) {
		return ofAsteroid(asteroid, 3.d, 14.d);
	}
	
	/**
	 * @param noiseLevel A higher noise value makes the astroid circumference 
	 * more noisy, i.e. the noise-peaks will be more noticeable.
	 * @param weight A higher weight makes the noise-peaks higher.
	 */
	private Shape ofAsteroid(Asteroid asteroid, double noiseLevel, double weight) {
		double center_x = asteroid.getX();
		double center_y = asteroid.getY();
		double volume = asteroid.getRadius();
		
		OpenSimplexNoise noise = new OpenSimplexNoise();
		
		Path2D path = new Path2D.Double();
		for (int angle = 0; angle < 360; angle = angle + 9) {
			double rads = Math.toRadians(angle);
			double x_offset = (Math.cos(rads) + 1) * noiseLevel;
			double y_offset = (Math.sin(rads) + 1) * noiseLevel;
			
			double noiseValue = noise.eval(x_offset, y_offset) * weight + 4;
			double x = center_x + (volume + noiseValue) * Math.cos(rads);
			double y = center_y + (volume + noiseValue) * Math.sin(rads);
			
			if (angle == 0)
				path.moveTo(x, y);
			else
				path.lineTo(x, y);
		}
		
		path.closePath();
		return path;
	}
	
	public Shape ofUfo(Ufo ufo) {
		final double center_x = ufo.getX();
		final double center_y = ufo.getY();
		final double r = ufo.getRadius();
		
		double b_diam = r * 2.d, m_diam = b_diam / 3.d;
		double bx = center_x - b_diam / 2.d, by = center_y - b_diam / 2.d;
		double mx = center_x - m_diam / 2.d, my = center_y - m_diam / 2.d;
		
		Ellipse2D ufoOuter = new Ellipse2D.Double(bx, by, b_diam, b_diam);
		Ellipse2D ufoInner = new Ellipse2D.Double(mx, my, m_diam, m_diam);
		
		Area area = new Area(ufoOuter);
		area.subtract(new Area(ufoInner));
		
		return area;
	}
	
}
