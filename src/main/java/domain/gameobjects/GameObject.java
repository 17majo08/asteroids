package domain.gameobjects;

import domain.factory.ShapeFactory;
import java.awt.Shape;
import java.awt.geom.Area;
import domain.geom2d.Point2d;
import domain.geom2d.Vector2d;

/**
 * Abstract class that holds methods that are the same across all GameObjects.
 *
 * @author Max Jonsson
 * @version 2020-02-21
 */
public abstract class GameObject implements AbstractGameObject {
	protected static final double ACCELERATION = 16.2d;
	
	protected final Point2d center;
	protected final double radius, density;
	protected final Vector2d directionVector;
	protected final ShapeFactory shapeFactory;
	
	public GameObject(double x, double y, double radius, double density) {
		this(x, y, radius, density, 0);
	}
	
	public GameObject(double x, double y, double radius, double density, double movingAngle) {
		this.center = new Point2d(x, y);
		this.radius = radius;
		this.density = density;
		this.directionVector = Vector2d.ofAngle(movingAngle, getTopSpeed());
		this.shapeFactory = new ShapeFactory();
	}
	
	/**
	 * Changes the direction and/or magnitude of the GameObject by affecting it 
	 * by another vector representing a force. Note that the GameObject has a
	 * max speed, so if the resulting force is too strong its magnitude will get
	 * reduced.
	 * @param force The other vector.
	 */
	protected void accelerate(Vector2d force) {
		directionVector.addForce(force);
		
		if (directionVector.getMagnitude() > getTopSpeed())
			directionVector.resize(getTopSpeed());
	}
	
	/**
	 * Stops the object from moving.
	 */
	protected void stopAcceleration() {
		directionVector.resize(0);
	}
	
	/**
	 * @return The top speed allowed for the GameObject.
	 */
	protected final double getTopSpeed() {
		return ACCELERATION / radius / density;
	}
	
	/**
	 * @param s1 The first Shape.
	 * @param s2 The second Shape.
	 * @return If the two shapes intersects.
	 */
	private static boolean intersects(Shape s1, Shape s2) {
		// Conditional for improved performance
		if (!s1.getBounds2D().intersects(s2.getBounds2D()))
			return false;
		
		Area area = new Area(s1);
		Area otherArea = new Area(s2);
		area.intersect(otherArea);
		return !area.isEmpty();
	}
	
	/**
	 * Template method.
	 * @return The Shape of the concrete GameObject.
	 */
	protected abstract Shape getShape();
	
	@Override
	public boolean hits(GameObject gameObject) {
		return intersects(getShape(), gameObject.getShape());
	}
	
	@Override
	public Vector2d getDirection() {
		return directionVector;
	}
	
	@Override
	public double getRadius() {
		return this.radius;
	}
	
	@Override
	public double getX() {
		return center.getX();
	}
	
	@Override
	public double getY() {
		return center.getY();
	}
	
	@Override
	public  Point2d getPosition() {
		return center.copy();
	}
	
	@Override
	public void moveTo(double x, double y) {
		center.moveTo(x, y);
	}
	
}
