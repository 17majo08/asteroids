package domain.gameobjects;

/**
 * Common interface for all GameObjects that the SpaceShip can shoot.
 *
 * @author Max Jonsson
 * @version 2020-02-27
 */
public interface Enemy {
	/**
	 * @return The number of points retrieved when this object is shot.
	 */
    public int getPoints();
}
