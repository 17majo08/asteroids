package domain.gameobjects;

import domain.state.Direction;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import domain.strategy.MoveStrategy;
import domain.geom2d.Vector2d;
import visitor.GameObjectVisitor;

/**
 * The SpaceShip. This is the GameObject controlled by the player.
 *
 * @author Max Jonsson
 * @version 2020-02-21
 */
public class SpaceShip extends GameObject implements Shooter {
	private static final double SHIP_RADIUS = 15d, SHIP_DENSITY = .2d;
	
	private final Vector2d facingVector;
	private final List<Bullet> bullets;
	private Direction rotationDirection;
	
	public SpaceShip(double x, double y) {
		super(x, y, SHIP_RADIUS, SHIP_DENSITY);
		super.stopAcceleration();
		
		this.facingVector = Vector2d.ofAngle(0, getTopSpeed());
		this.bullets = new CopyOnWriteArrayList<>();
		this.rotationDirection = Direction.NONE;
	}
	
	/**
	 * Makes the SpaceShip dash in the direction it's currently facing.
	 */
	public void dash() {
		super.accelerate(facingVector);
	}
	
	/**
	 * @return The angle the SpaceShip is currently facing.
	 */
	public double getFacingAngle() {
		return facingVector.getDegrees();
	}
	
	/**
	 * Set the rotation direction.
	 * @param direction The new rotation direction.
	 */
	public void setRotationDirection(Direction direction) {
		this.rotationDirection = direction;
	}
	
	/**
	 * @return The current rotation direction.
	 */
	public Direction getRotationDirection() {
		return this.rotationDirection;
	}
	
	@Override
	public void shoot() {
		bullets.add(new Bullet(getX(), getY(), getFacingAngle()));
	}
	
	@Override
	public void removeBullet(Bullet bullet) {
		bullets.remove(bullet);
	}
	
	@Override
	public List<Bullet> getBullets() {
		return this.bullets;
	}
	
	@Override
	public void move() {
		facingVector.turn(rotationDirection.getDegrees());
		MoveStrategy.withFlip().move(this);
		bullets.forEach(bullet -> bullet.move());
	}
	
	@Override
	protected Shape getShape() {
		AffineTransform tx = new AffineTransform();
		tx.rotate(Math.toRadians(-getFacingAngle()), getX(), getY());
		
		Shape shape = shapeFactory.ofSpaceShip(this);
		return tx.createTransformedShape(shape);
	}
	
	@Override
	public void accept(GameObjectVisitor visitor) {
		visitor.visitSpaceShip(this);
	}
	
}
