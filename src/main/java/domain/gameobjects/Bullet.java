package domain.gameobjects;

import java.awt.Shape;
import domain.strategy.MoveStrategy;
import visitor.GameObjectVisitor;

/**
 * The Bullet used by all GameObjects that are Shooters.
 * 
 * @author Max Jonsson
 * @version 2020-02-20
 */
public class Bullet extends GameObject {
	private static final double BULLET_RADIUS = 4.d, BULLET_DENSITY = .3d;
	
	public Bullet(double x, double y, double movingAngle) {
		super(x, y, BULLET_RADIUS, BULLET_DENSITY, movingAngle);
	}
	
	@Override
	public void move() {
		MoveStrategy.noFlip().move(this);
	}
	
	@Override
	protected Shape getShape() {
		return shapeFactory.ofBullet(this);
	}
	
	@Override
	public void accept(GameObjectVisitor visitor) {
		visitor.visitBullet(this);
	}
    
}
