package domain.gameobjects;

import java.util.List;
import domain.factory.SpawnFactory;
import java.awt.Shape;
import visitor.GameObjectVisitor;

/**
 * The bigger sized Asteroid.
 * 
 * @author Max Jonsson
 * @version 2020-02-24
 */
public class AsteroidBig extends Asteroid implements Enemy {
	private static final double ASTEROID_RADIUS = 48.d;
	private static final int POINTS = 20;
	
	public AsteroidBig(double x, double y, double movingAngle) {
		super(x, y, ASTEROID_RADIUS, movingAngle);
	}
	
	@Override
	public List<Asteroid> destroy() {
		return SpawnFactory.ASTEROID_MEDIUM.create(center.getX(), center.getY(), 2);
	}
	
	@Override
	protected Shape getShape() {
		return shapeFactory.ofAsteroidBig(this);
	}
	
	@Override
	public void accept(GameObjectVisitor visitor) {
		visitor.visitAsteroidBig(this);
	}

	@Override
	public int getPoints() {
		return POINTS;
	}
    
}
