package domain.gameobjects;

import java.util.List;
import domain.factory.SpawnFactory;
import java.awt.Shape;
import visitor.GameObjectVisitor;

/**
 * The medium sized Asteroid.
 * 
 * @author Max Jonsson
 * @version 2020-02-24
 */
public class AsteroidMedium extends Asteroid {
    private static final double ASTEROID_RADIUS = 24.d;
	private static final int POINTS = 50;
	
	public AsteroidMedium(double x, double y, double movingAngle) {
		super(x, y, ASTEROID_RADIUS, movingAngle);
	}

	@Override
	public List<Asteroid> destroy() {
		return SpawnFactory.ASTEROID_SMALL.create(center.getX(), center.getY(), 2);
	}
	
	@Override
	protected Shape getShape() {
		return shapeFactory.ofAsteroidMedium(this);
	}
	
	@Override
	public void accept(GameObjectVisitor visitor) {
		visitor.visitAsteroidMedium(this);
	}
	
	@Override
	public int getPoints() {
		return POINTS;
	}
}
