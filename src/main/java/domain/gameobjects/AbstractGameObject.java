package domain.gameobjects;

import domain.geom2d.Point2d;
import domain.geom2d.Vector2d;
import visitor.VisitableGameObject;

/**
 * Common interface for all GameObjects.
 *
 * @author Max Jonsson
 * @version 2020-02-21
 */
public interface AbstractGameObject extends VisitableGameObject {
	public double getX();
	public double getY();
	public double getRadius();
	/**
	 * @return The current position of this object.
	 */
	public Point2d getPosition();
	/**
	 * @return The direction this object is currently moving in.
	 */
	public Vector2d getDirection();
	/**
	 * Moves this object one time step in its current direction.
	 */
	public void move();
	/**
	 * Moves this object to the sent in coordinate.
	 * @param x The x-coordinate.
	 * @param y The y-coordinate.
	 */
	public void moveTo(double x, double y);
	/**
	 * Returns if this object collides with the sent in GameObject.
	 * @param gameObject The other GameObject.
	 * @return True or false.
	 */
	public boolean hits(GameObject gameObject);
}
