package domain.gameobjects;

import java.util.List;

/**
 * Common interface for all GameObjects that can shoot Bullets.
 *
 * @author Max Jonsson
 * @version 2020-02-25
 */
public interface Shooter {
	/**
	 * Shoots a Bullet.
	 */
    public void shoot();
	/**
	 * Removes the Bullet.
	 * @param bullet The Bullet to remove.
	 */
	public void removeBullet(Bullet bullet);
	/**
	 * @return All Bullets owned by this object.
	 */
	public List<Bullet> getBullets();
}
