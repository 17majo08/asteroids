package domain.gameobjects;

import java.awt.Shape;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import visitor.GameObjectVisitor;

/**
 * The smaller size Asteroid.
 * 
 * @author Max Jonsson
 * @version 2020-02-24
 */
public class AsteroidSmall extends Asteroid {
    private static final double ASTEROID_RADIUS = 12.d;
	private static final int POINTS = 100;
	
	public AsteroidSmall(double x, double y, double movingAngle) {
		super(x, y, ASTEROID_RADIUS, movingAngle);
	}

	@Override
	public List<Asteroid> destroy() {
		return new CopyOnWriteArrayList<>();
	}
	
	@Override
	public void accept(GameObjectVisitor visitor) {
		visitor.visitAsteroidSmall(this);
	}
	
	@Override
	protected Shape getShape() {
		return shapeFactory.ofAsteroidSmall(this);
	}
	
	@Override
	public int getPoints() {
		return POINTS;
	}
}
