package domain.gameobjects;

import java.awt.Shape;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import domain.strategy.MoveStrategy;
import visitor.GameObjectVisitor;

/**
 * The Ufo.
 * 
 * @author Max Jonsson
 * @version 2020-02-24
 */
public class Ufo extends GameObject implements Shooter, Enemy {
	private static final double UFO_RADIUS = 18.d, UFO_DENSITY = .4d;
	private static final int POINTS = 200;
	
	private final List<Bullet> bullets;
	
	public Ufo(double x, double y, double movingAngle) {
		super(x, y, UFO_RADIUS, UFO_DENSITY, movingAngle);
		
		this.bullets = new CopyOnWriteArrayList<>();
	}
	
	/**
	 * Shoots a homing shot.
	 * @param angle The angle in which to shoot.
	 */
	public void shoot(double angle) {
		bullets.add(new Bullet(getX(), getY(), angle));
	}
	
	@Override
	public void shoot() {
		shoot(getDirection().getDegrees());
	}
	
	@Override
	public void removeBullet(Bullet bullet) {
		bullets.remove(bullet);
	}

	@Override
	public List<Bullet> getBullets() {
		return this.bullets;
	}
	
	@Override
	public void move() {
		MoveStrategy.withFlip().move(this);
		bullets.forEach(bullet -> bullet.move());
	}
	
	@Override
	protected Shape getShape() {
		return shapeFactory.ofUfo(this);
	}
	
	@Override
	public void accept(GameObjectVisitor visitor) {
		visitor.visitUfo(this);
	}

	@Override
	public int getPoints() {
		return POINTS;
	}
	
}
