package domain.gameobjects;

import java.util.List;
import domain.strategy.MoveStrategy;

/**
 * 
 * 
 * @author Max Jonsson
 * @version 2020-02-21
 */
public abstract class Asteroid extends GameObject implements Enemy {
	protected static final double ASTEROID_DENSITY = .3d;
	
	public Asteroid(double x, double y, double volume, double movingAngle) {
		super(x, y, volume, ASTEROID_DENSITY, movingAngle);
	}
	
	/**
	 * Splits this Asteroid into smaller Asteroids.
	 * @return A list of smaller Asteroids.
	 */
	public abstract List<Asteroid> destroy();
	
	@Override
	public void move() {
		MoveStrategy.withFlip().move(this);
	}
    
}
