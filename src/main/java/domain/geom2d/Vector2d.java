package domain.geom2d;

/**
 * Representation of a 2-dimensional vector.
 *
 * @author Max Jonsson
 * @version 2020-02-22
 */
public class Vector2d implements Clonable {
	private double x, y;
	
	private Vector2d(double x, double y) {
		setCoordinates(x, y);
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public final void setCoordinates(double x, double y) {
		setX(x);
		setY(y);
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getDegrees() {
		return Math.toDegrees(getRadians());
	}
	
	public double getRadians() {
		return Math.atan2(getY(), getX());
	}
	
	public double getMagnitude() {
		return Math.sqrt(getX() * getX() + getY() * getY());
	}
	
	/**
	 * Adds another vector to this vector.
	 * @param v2 The other vector.
	 */
	public void addForce(Vector2d v2) {
		setX(getX() + v2.getX());
		setY(getY() + v2.getY());
	}
	
	/**
	 * Rotates the vector.
	 * @param angle The angle to rotate by.
	 */
	public void turn(double angle) {
		double radians = Math.toRadians(angle);
		double x2 = getX() * Math.cos(radians) - getY() * Math.sin(radians);
		double y2 = getX() * Math.sin(radians) + getY() * Math.cos(radians);
		
		setCoordinates(x2, y2);
	}
	
	/**
	 * Resizes the vector.
	 * @param magnitude The new size.
	 */
	public void resize(double magnitude) {
		double scaling = magnitude / getMagnitude();
		
		setCoordinates(getX() * scaling, getY() * scaling);
	}
	
	/**
	 * Creates a vector with specified angle. The returning vector will be 
	 * normalized.
	 * @param degrees The angle of the vector.
	 * @return The vector.
	 */
	public static Vector2d ofAngle(double degrees) {
		return ofAngle(degrees, 1);
	}
	
	/**
	 * Creates a vector with specified angle and specified magnitude.
	 * @param degrees The angle of the vector.
	 * @param magnitude The magnitude of the vector.
	 * @return The vector.
	 */
	public static Vector2d ofAngle(double degrees, double magnitude) {
		double radians = Math.toRadians(degrees);
		
		return new Vector2d(magnitude * Math.cos(radians), magnitude * Math.sin(radians));
	}
	
	/**
	 * Creates a vector from the origin to the sent in coordinate.
	 * @param x The x-coordinate.
	 * @param y The y-coordinate.
	 * @return The vector.
	 */
	public static Vector2d ofCoordinates(double x, double y) {
		return new Vector2d(x, y);
	}
	
	@Override
	public Vector2d copy() {
		return Vector2d.ofCoordinates(getX(), getY());
	}
	
}
