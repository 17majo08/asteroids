package domain.geom2d;

/**
 * Implementing this interface makes an object able to copy itself. This allows
 * an object to return an instance of the same type and with the same inner 
 * content, but where the individual objects share no references.
 *
 * @author Max Jonsson
 * @version 2020-02-04
 */
public interface Clonable {
    /**
	 * Copies the object.
	 * @param <T> The type to clone.
	 * @return A deep copy of the object.
	 */
	public <T> T copy();
}
