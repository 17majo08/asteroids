package domain.geom2d;

/**
 * Keeps track of a two dimensional point.
 *
 * @author Max Jonsson
 * @version 2020-02-10
 */
public class Point2d implements Clonable {
	private double x, y;
	
	/**
	 * Creates a point.
	 * @param x The x-coordinate.
	 * @param y The y-coordinate.
	 */
	public Point2d(double x, double y) {
		setX(x);
		setY(y);
	}
	
	/**
	 * @param x The x-coordinate.
	 */
	public final void setX(double x) {
		this.x = x;
	}
	
	/**
	 * @param y The y-coordinate.
	 */
	public final void setY(double y) {
		this.y = y;
	}
	
	/**
	 * @return The x-coordinate. 
	 */
	public double getX() {
		return this.x;
	}
	
	/**
	 * @return The y-coordinate.
	 */
	public double getY() {
		return this.y;
	}
	
	/**
	 * Moves the point relative its current location.
	 * @param dx By how many distances to move the x-coordinate.
	 * @param dy By how many distances to move the y-coordinate.
	 */
	public void move(double dx, double dy) {
		setX(getX() + dx);
		setY(getY() + dy);
	}
	
	public void moveTo(double x, double y) {
		setX(x);
		setY(y);
	}
	
	/**
	 * The euclidean distance between this point and the x- & y-coordinates.
	 * @param x The x-coordinate.
	 * @param y The y-coordinate.
	 * @return The euclidean distance.
	 */
	public double distanceTo(double x, double y) {
		double dx = getX() - x;
		double dy = getY() - y;
		
		return Math.sqrt(dx * dx + dy * dy);
	}
	
	/**
	 * The euclidean distance between this and another point.
	 * @param other The other point.
	 * @return The euclidean distance.
	 */
	public double distanceTo(Point2d other) {
		return distanceTo(other.getX(), other.getY());
	}
	
	/**
	 * The angle between this and another point.
	 * @param other The other point.
	 * @return The angle.
	 */
	public double angleTo(Point2d other) {
		double diff_x = getX() - other.getX();
		double diff_y = other.getY() - getY();
		return Math.toDegrees(Math.atan2(diff_y, diff_x)) + 180d;
	}

	@Override
	public Point2d copy() {
		return new Point2d(x, y);
	}
	
}
