package domain.strategy;

import domain.gameobjects.GameObject;
import gui.Bounds;
import domain.geom2d.Vector2d;

/**
 * Common interface for the Strategies in which GameObjects can move.
 *
 * @author Max Jonsson
 * @version 2020-03-17
 */
public interface MoveStrategy {
	/**
	 * Moves the GameObject.
	 * @param gameObject The GameObject to be moved.
	 */
	public void move(GameObject gameObject);
	
	/**
	 * Returns a MoveStrategy that makes the GameObject stay within the Game area
	 * when it leaves its bounds.
	 * @return The MoveStrategy.
	 */
	public static MoveStrategy withFlip() {
		return gameObject -> {
			Vector2d directionVector = gameObject.getDirection();
			
			double x = (gameObject.getX() + directionVector.getX() + Bounds.WIDTH) % Bounds.WIDTH;
			double y = (gameObject.getY() - directionVector.getY() + Bounds.HEIGHT) % Bounds.HEIGHT;
			gameObject.moveTo(x, y);
		};
	}
	
	/**
	 * Returns a MoveStrategy that allows the GameObject to leave the bounds of
	 * the Game area.
	 * @return The MoveStrategy.
	 */
	public static MoveStrategy noFlip() {
		return gameObject -> {
			Vector2d directionVector = gameObject.getDirection();
			
			double x = gameObject.getX() + directionVector.getX();
			double y = gameObject.getY() - directionVector.getY();
			gameObject.moveTo(x, y);
		};
	}
}
