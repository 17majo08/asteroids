package domain.state;

import domain.factory.SpawnFactory;
import domain.gameobjects.Asteroid;
import domain.gameobjects.Ufo;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Enumeration for all Levels of the Game.
 *
 * @author Max Jonsson
 * @version 2020-02-23
 */
public enum Level {
	ONE {
		@Override
		public Level proceed() {
			return Level.TWO;
		}
		
		@Override
		public List<Asteroid> spawnAsteroids() {
			List<Asteroid> asteroids = new CopyOnWriteArrayList<>();
			asteroids.addAll(SpawnFactory.ASTEROID_SMALL.create(1));
			asteroids.addAll(SpawnFactory.ASTEROID_MEDIUM.create(1));
			asteroids.addAll(SpawnFactory.ASTEROID_BIG.create(1));
			return asteroids;
		}

		@Override
		public List<Ufo> spawnUfos() {
			return SpawnFactory.UFO.create(1);
		}
	}, TWO {
		@Override
		public Level proceed() {
			return Level.THREE;
		}
		
		@Override
		public List<Asteroid> spawnAsteroids() {
			List<Asteroid> asteroids = new CopyOnWriteArrayList<>();
			asteroids.addAll(SpawnFactory.ASTEROID_SMALL.create(2));
			asteroids.addAll(SpawnFactory.ASTEROID_MEDIUM.create(2));
			asteroids.addAll(SpawnFactory.ASTEROID_BIG.create(2));
			return asteroids;
		}

		@Override
		public List<Ufo> spawnUfos() {
			return SpawnFactory.UFO.create(1);
		}
	}, THREE {
		@Override
		public Level proceed() {
			return Level.ONE;
		}
		
		@Override
		public List<Asteroid> spawnAsteroids() {
			List<Asteroid> asteroids = new CopyOnWriteArrayList<>();
			asteroids.addAll(SpawnFactory.ASTEROID_SMALL.create(3));
			asteroids.addAll(SpawnFactory.ASTEROID_MEDIUM.create(3));
			asteroids.addAll(SpawnFactory.ASTEROID_BIG.create(3));
			return asteroids;
		}

		@Override
		public List<Ufo> spawnUfos() {
			return SpawnFactory.UFO.create(1);
		}
	};
	
	/**
	 * @return The Level state to which to proceed when current level is 
	 * completed.
	 */
	public abstract Level proceed();
	/**
	 * Spawns all the Asteroids included in current Level.
	 * @return A list of Asteroids.
	 */
	public abstract List<Asteroid> spawnAsteroids();
	/**
	 * Spawns all the Ufos included in current Level.
	 * @return A list of Ufos.
	 */
	public abstract List<Ufo> spawnUfos();
}
