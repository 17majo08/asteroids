package domain.state;

import visitor.GameStateVisitor;
import visitor.VisitableGameState;

/**
 * The GameState represents which state the Game is currently in. The GameStates 
 * are Visitable so a GameStateVisitor can perform actions specifically to a 
 * certain state.
 * 
 * @author Max Jonsson
 * @version 2020-03-05
 */
public enum GameState implements VisitableGameState {
	NOT_STARTED {
		@Override
		public void accept(GameStateVisitor visitor) {
			visitor.visitNotStarted();
		}
	}, STARTED {
		@Override
		public void accept(GameStateVisitor visitor) {
			visitor.visitStarted();
		}
	}, GAME_OVER {
		@Override
		public void accept(GameStateVisitor visitor) {
			visitor.visitGameOver();
		}
	};
	
	private static GameState gameState = NOT_STARTED;
	
	public static GameState getGameState() {
		return gameState;
	}
	
	public static void setGameState(GameState state) {
		gameState = state;
	}
	
}
