package domain.state;

/**
 * State enumeration that represents in which Direction the SpaceShip currently 
 * is rotating in. The rotation Direction changes based on key events. All 
 * enumerations provides a method for each specific key action to change the 
 * state in proper way.
 *
 * @author Max Jonsson
 * @version 2020-02-23
 */
public enum Direction {
	RIGHT(-4.4d) {
		@Override
		public Direction onLeftDown() {
			return Direction.BOTH;
		}

		@Override
		public Direction onRightDown() {
			return Direction.RIGHT;
		}

		@Override
		public Direction onLeftUp() {
			return Direction.RIGHT;
		}

		@Override
		public Direction onRightUp() {
			return Direction.NONE;
		}
	}, LEFT(4.4d) {
		@Override
		public Direction onLeftDown() {
			return Direction.LEFT;
		}

		@Override
		public Direction onRightDown() {
			return Direction.BOTH;
		}

		@Override
		public Direction onLeftUp() {
			return Direction.NONE;
		}

		@Override
		public Direction onRightUp() {
			return Direction.LEFT;
		}
	}, NONE(0d) {
		@Override
		public Direction onLeftDown() {
			return Direction.LEFT;
		}

		@Override
		public Direction onRightDown() {
			return Direction.RIGHT;
		}

		@Override
		public Direction onLeftUp() {
			return Direction.NONE;
		}

		@Override
		public Direction onRightUp() {
			return Direction.NONE;
		}
	}, BOTH(0d) {
		@Override
		public Direction onLeftDown() {
			return Direction.BOTH;
		}

		@Override
		public Direction onRightDown() {
			return Direction.BOTH;
		}

		@Override
		public Direction onLeftUp() {
			return Direction.RIGHT;
		}

		@Override
		public Direction onRightUp() {
			return Direction.LEFT;
		}
	};
	
	private final double degrees;
	
	private Direction(double degrees) {
		this.degrees = degrees;
	}
	
	public double getDegrees() {
		return this.degrees;
	}
	
	public abstract Direction onLeftDown();
	public abstract Direction onRightDown();
	public abstract Direction onLeftUp();
	public abstract Direction onRightUp();
}
