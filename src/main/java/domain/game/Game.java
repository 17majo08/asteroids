package domain.game;

import dao.HighScoreDAO;
import domain.state.GameState;
import domain.state.Direction;
import domain.gameobjects.SpaceShip;
import domain.gameobjects.GameObject;
import domain.gameobjects.Ufo;
import domain.gameobjects.Asteroid;
import gui.Bounds;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.Timer;
import domain.state.Level;
import observer.Observer;
import observer.Subject;

/**
 * This is the Game object that manages over all the Game specific objects. 
 * It also holds the main game loop that moves all the objects and checks for 
 * collisions.
 *
 * @author Max Jonsson
 * @version 2020-02-21
 */
public class Game implements Subject {
	private SpaceShip spaceShip;
	private List<Asteroid> asteroids;
	private List<Ufo> ufos;
	
	private Timer gameLoopTimer;
	private Level level;
	private final List<Observer> observers;
	private boolean isStartable;
	
	private final HighScoreDAO highScoreDAO;
	private int score, highScore;
	
	public Game() {
		this.observers = new CopyOnWriteArrayList<>();
		this.highScoreDAO = new HighScoreDAO();
		this.highScore = highScoreDAO.readHighScore();
		init();
	}
	
	/**
	 * Initiates the game variables to prepare for start/restart.
	 */
	private void init() {
		this.spaceShip = new SpaceShip(Bounds.WIDTH / 2, Bounds.HEIGHT / 2);
		this.asteroids = new CopyOnWriteArrayList<>();
		this.ufos = new CopyOnWriteArrayList<>();
		
		this.score = 0;
		this.level = Level.ONE;
		
		this.gameLoopTimer = new Timer(30, e -> moveObjects());
		this.isStartable = true;
	}
	
	/**
	 * The default method run by the game loop. It moves all the GameObjects and
	 * checks for collisions and for level completed. It also updates the 
	 * observers on the changed state.
	 */
	private void moveObjects() {
		spaceShip.move();
		ufos.forEach(ufo -> ufo.move());
		asteroids.forEach(asteroid -> asteroid.move());
		
		checkForCollisions();
		checkEnemiesCleared();
		
		notifyObservers();
	}
	
	private void checkEnemiesCleared() {
		Stream.concat(asteroids.stream(), ufos.stream())
				.findAny()
				.ifPresentOrElse(obj -> {}, () -> levelUp());
	}
	
	private void levelUp() {
		this.level = level.proceed();
		spawnGameObjects();
	}
	
	private void checkForCollisions() {
		List<GameObject> spaceShipEnemies = new CopyOnWriteArrayList<>();
		spaceShipEnemies.addAll(asteroids);
		spaceShipEnemies.addAll(ufos);
		spaceShipEnemies.addAll(ufos.stream()
				.flatMap(ufo -> ufo.getBullets().stream())
				.collect(Collectors.toList()));
		
		spaceShipEnemies.stream()
				.filter(enemy -> spaceShip.hits(enemy))
				.findAny().ifPresent(enemy -> endGame());
		
		spaceShip.getBullets().forEach(bullet -> {
			ufos.stream()
					.filter(ufo -> ufo.hits(bullet))
					.findAny().ifPresent(ufo -> {
						score += ufo.getPoints();
						ufos.remove(ufo);
						spaceShip.removeBullet(bullet);
					});
			asteroids.stream()
					.filter(asteroid -> asteroid.hits(bullet))
					.findAny().ifPresent(asteroid -> {
						score += asteroid.getPoints();
						asteroids.remove(asteroid);
						asteroids.addAll(asteroid.destroy());
						spaceShip.removeBullet(bullet);
					});
		});
	}
	
	private void spawnGameObjects() {
		asteroids.addAll(level.spawnAsteroids());
		ufos.addAll(level.spawnUfos());
		
		ufos.forEach(ufo -> {
			int millis = (int) (Math.random() * 4000 + 1000);
			Timer shotTimer = new Timer(millis, e -> {
				ufo.shoot(ufo.getPosition().angleTo(spaceShip.getPosition()));
			});
			shotTimer.start();
		});
	}
	
	public void startGame() {
		if (isStartable) {
			init();
			spawnGameObjects();
			gameLoopTimer.start();
			GameState.setGameState(GameState.STARTED);
		}
	}
	
	public boolean isStarted() {
		return gameLoopTimer.isRunning();
	}
	
	public int getHighScore() {
		return this.highScore;
	}
	
	public int getScore() {
		return this.score;
	}
	
	private void endGame() {
		gameLoopTimer.stop();
		GameState.setGameState(GameState.GAME_OVER);
		
		if (score > highScore) {
			this.highScore = score;
			highScoreDAO.writeHighScore(score);
		}
		
		isStartable = false;
		Timer t = new Timer(1500, e -> isStartable = true);
		t.setRepeats(false);
		t.start();
	}
	
	public void shoot() {
		spaceShip.shoot();
	}
	
	public void dash() {
		spaceShip.dash();
	}
	
	public Direction getRotationDirection() {
		return spaceShip.getRotationDirection();
	}
	
	public void setRotationDirection(Direction direction) {
		spaceShip.setRotationDirection(direction);
	}
	
	public List<GameObject> getGameObjects() {
		List<GameObject> objects = new CopyOnWriteArrayList<>();
		objects.add(spaceShip);
		objects.addAll(asteroids);
		objects.addAll(ufos);
		
		return objects;
	}
	
	@Override
	public void addObserver(Observer observer) {
		observers.add(observer);
	}
	
	@Override
	public void notifyObservers() {
		observers.forEach(observer -> observer.update());
	}
	
}
